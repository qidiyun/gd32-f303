/*!
    \file  readme.txt
    \brief description of the GPIO_Running_Led example
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

  This demo is based on the GD32303E-EVAL-V1.0 board, LED2 connected to PF0. LED3 
connected to PF1. LED4 connected to PF2. Then, three LEDs can light cycles.
