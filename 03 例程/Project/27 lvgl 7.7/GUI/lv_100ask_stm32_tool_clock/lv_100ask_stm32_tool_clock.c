/**
 ******************************************************************************
 * @file    lv_100ask_stm32_tool_clock.c
 * @author  百问科技
 * @version V1.2
 * @date    2020-12-12
 * @brief	计算器应用
 ******************************************************************************
 * Change Logs:
 * Date           Author          Notes
 * 2020-12-12     zhouyuebiao     First version
 * 2021-01-25     zhouyuebiao     V1.2 
 ******************************************************************************
 * @attention
 *
 * Copyright (C) 2008-2021 深圳百问网科技有限公司<https://www.100ask.net/>
 * All rights reserved
 *
 ******************************************************************************
 */

/*********************
 *      INCLUDES
 *********************/
#include <stdio.h>
#include <stdlib.h>
#include "lv_100ask_stm32_tool_clock.h"


/**********************
 *  STATIC VARIABLES
 **********************/
static PT_lv_100ask_clock g_pt_lv_100ask_clock;  // 结构体


/**********************
 *  STATIC PROTOTYPES
 **********************/
static void lv_100ask_stm32_tool_clock_init(void);										// 界面初始化
static void lv_100ask_stm32_tool_clock_anim(lv_obj_t * gauge, lv_anim_value_t value);	// 时钟动画
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event);				// 返回桌面事件处理函数


/*
 *  函数名：   void lv_100ask_stm32_tool_clock(void)
 *  输入参数： 无
 *  返回值：   无
 *  函数作用： 应用初始化入口
*/
void lv_100ask_stm32_tool_clock(void)
{
	g_pt_lv_100ask_clock = (T_lv_100ask_clock *)malloc(sizeof(T_lv_100ask_clock));   // 申请内存

	g_pt_lv_100ask_clock->bg_tool_clock = lv_obj_create(lv_scr_act(), NULL);
	lv_obj_set_size(g_pt_lv_100ask_clock->bg_tool_clock, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_y(g_pt_lv_100ask_clock->bg_tool_clock, 0);


    lv_100ask_stm32_tool_clock_init();

//    add_title(g_pt_lv_100ask_clock->bg_tool_clock, "CLOCK");
//	add_back(g_pt_lv_100ask_clock->bg_tool_clock, event_handler_back_to_home);   // 返回主菜单
}


/*
 *  函数名：   static void lv_100ask_stm32_tool_clock_init(void)
 *  输入参数： 无
 *  返回值：   无
 *  函数作用： 应用界面初始化
*/
static void lv_100ask_stm32_tool_clock_init(void)
{
	LV_IMG_DECLARE(img_hand);
	
	lv_anim_t anmi_gauge_clock;
    lv_anim_init(&anmi_gauge_clock);
    lv_anim_set_values(&anmi_gauge_clock, 0, 60);
    lv_anim_set_time(&anmi_gauge_clock, 60000);
    lv_anim_set_repeat_count(&anmi_gauge_clock, LV_ANIM_REPEAT_INFINITE);
    lv_anim_start(&anmi_gauge_clock);

    /*Describe the color for the needles*/
    static lv_color_t needle_colors[3];
    needle_colors[0] = LV_COLOR_ORANGE;
    needle_colors[1] = LV_COLOR_BLUE;
    needle_colors[2] = LV_COLOR_PURPLE;    

    /*Create a gauge*/
    g_pt_lv_100ask_clock->g_gauge_clock = lv_gauge_create(g_pt_lv_100ask_clock->bg_tool_clock, NULL);
    lv_obj_set_style_local_bg_color(g_pt_lv_100ask_clock->g_gauge_clock, LV_GAUGE_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_BLACK);  // text
    lv_obj_set_style_local_text_opa(g_pt_lv_100ask_clock->g_gauge_clock, LV_GAUGE_PART_MAIN, LV_STATE_DEFAULT, LV_OPA_0);  // text
    lv_obj_set_style_local_line_opa(g_pt_lv_100ask_clock->g_gauge_clock, LV_GAUGE_PART_MAIN, LV_STATE_DEFAULT, LV_OPA_0);  // text
    lv_gauge_set_range(g_pt_lv_100ask_clock->g_gauge_clock, 0 ,60);
    lv_gauge_set_angle_offset(g_pt_lv_100ask_clock->g_gauge_clock, 180);
    lv_gauge_set_scale(g_pt_lv_100ask_clock->g_gauge_clock, 360, 60, 13);
    lv_gauge_set_needle_count(g_pt_lv_100ask_clock->g_gauge_clock, 3, needle_colors);
    lv_obj_set_size(g_pt_lv_100ask_clock->g_gauge_clock, 250, 250);
    lv_obj_set_drag_parent(g_pt_lv_100ask_clock->g_gauge_clock, true);
    lv_obj_align(g_pt_lv_100ask_clock->g_gauge_clock, NULL, LV_ALIGN_CENTER, 0, 0);
    lv_gauge_set_needle_img(g_pt_lv_100ask_clock->g_gauge_clock, &img_hand, 4, 4);

    /*Allow recoloring of the images according to the needles' color*/
    lv_obj_set_style_local_image_recolor_opa(g_pt_lv_100ask_clock->g_gauge_clock, LV_GAUGE_PART_NEEDLE, LV_STATE_DEFAULT, LV_OPA_COVER);


    g_pt_lv_100ask_clock->g_label_clock_time = lv_label_create(g_pt_lv_100ask_clock->bg_tool_clock, NULL);
    lv_obj_set_style_local_text_font(g_pt_lv_100ask_clock->g_label_clock_time, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, &lv_font_montserrat_32);  	// text font
    lv_label_set_text_fmt(g_pt_lv_100ask_clock->g_label_clock_time, "%s:%s:%s", "09", "00", "00");
    lv_obj_align(g_pt_lv_100ask_clock->g_label_clock_time, g_pt_lv_100ask_clock->g_gauge_clock, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);

    /*Set the values*/
    lv_gauge_set_value(g_pt_lv_100ask_clock->g_gauge_clock, 2, 45);
    lv_anim_set_var(&anmi_gauge_clock, g_pt_lv_100ask_clock->g_gauge_clock);
    lv_anim_set_exec_cb(&anmi_gauge_clock, (lv_anim_exec_xcb_t)lv_100ask_stm32_tool_clock_anim);
    lv_anim_start(&anmi_gauge_clock);
}


/*
 *  函数名：   static void lv_100ask_stm32_tool_clock_anim(lv_obj_t * obj, lv_anim_value_t value)
 *  输入参数： 动画对象
 *  输入参数： 传递给动画的数据
 *  返回值：   无
 *  函数作用： 更新时分秒、表盘指针位置
*/
static void lv_100ask_stm32_tool_clock_anim(lv_obj_t * gauge, lv_anim_value_t value)
{
    static int32_t hours = 48, minutes = 41; // Hours, minutes, seconds

    lv_gauge_set_value(gauge, 0, value);

    if (value >= 60)
    {
        value = 0;
        minutes++;

        if ((minutes % 12) == 0)
        {
            hours++;
        }
        if (minutes >= 60)
        {
            minutes = 0;
        }
        if (hours >= 60)
        {
            hours = 0;
            minutes = 0;
        }
    }

    lv_gauge_set_value(gauge, 1, minutes);
    lv_gauge_set_value(gauge, 2, hours);

    char tmpc[32] = {0}; // 保持 00:00:00 的格式
    strcat(tmpc, (hours / 5) >=10 ? "%d:":"0%d:");
    strcat(tmpc, (minutes)   >=10 ? "%d:":"0%d:");
    strcat(tmpc, (value)     >=10 ? "%d":"0%d");

    lv_label_set_text_fmt(g_pt_lv_100ask_clock->g_label_clock_time, tmpc, (hours / 5), minutes, value);
}


/*
 *  函数名：   static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
 *  输入参数： 触发事件的对象
 *  输入参数： 触发的事件类型
 *  返回值：   无
 *  函数作用： 返回桌面事件处理函数
*/
static void event_handler_back_to_home(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED)
    {
		/* 删除表盘 */
		if (g_pt_lv_100ask_clock->g_gauge_clock != NULL)
		{
			lv_anim_del(g_pt_lv_100ask_clock->g_gauge_clock, (lv_anim_exec_xcb_t)lv_100ask_stm32_tool_clock_anim);   // 删除动画
			lv_obj_del(g_pt_lv_100ask_clock->g_gauge_clock);
		}
		
		/* 删除背景 */
        if (g_pt_lv_100ask_clock->bg_tool_clock != NULL)	lv_obj_del(g_pt_lv_100ask_clock->bg_tool_clock);
		
		/* 释放内存 */
		free(g_pt_lv_100ask_clock);
		
		/* 清空屏幕，返回桌面 */
//        lv_100ask_stm32_anim_out_all(lv_scr_act(), 0);
 //       lv_100ask_stm32_demo_home(10);
    }
}
