#define __DAC_C__

#include <gd32f30x.h>
#include "dac.h"

//输出的DAC值
#define DAC_OUT_VAL    (0x7FF0)


/*!
打开相关时钟
*/
void dac_rcu_config(void)
{
    /* enable the clock of peripherals */
    rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_DAC);
}

/*!
配置DAC引脚
*/
void dac_gpio_config(void)
{
    /* once enabled the DAC, the corresponding GPIO pin is connected to the DAC converter automatically */
    gpio_init(GPIOA, GPIO_MODE_AIN, GPIO_OSPEED_50MHZ, GPIO_PIN_4);
}

/*!
DAC配置
*/
void dac_config(void)
{
    dac_deinit();
    /* configure the DAC0 */
    dac_trigger_disable(DAC0);
    dac_wave_mode_config(DAC0, DAC_WAVE_DISABLE);
    dac_output_buffer_enable(DAC0);
    
    /* enable DAC0 and set data */
    dac_enable(DAC0);

	//输出DAC值
    dac_data_set(DAC0, DAC_ALIGN_12B_L, DAC_OUT_VAL);
}

//初始化
void dac_init(void)
{
	dac_rcu_config();
	dac_gpio_config();
	dac_config();
}


