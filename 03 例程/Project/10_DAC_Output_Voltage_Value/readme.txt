/*!
    \file  readme.txt
    \brief description of DAC_Output_Voltage_Value example
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

  This demo is based on the GD32303E-EVAL-V1.0 board, it shows how to use DAC to output voltage. 
Firstly, all the LEDs are turned on and off for test. No trigger source is chosen to trigger
DAC. The DAC0 output pin is configured as PA4. The voltage of PA4 is VREF/2.
