#ifndef __KEY_H__
#define __KEY_H__

#include <gd32f30x.h>

void  key_init (void);
uint8_t key_state_get(void);
void  key_exit_mode_init (void);


#endif

