#ifndef __CAN_H__
#define __CAN_H__

#ifdef __CAN_C__
#define __CAN_EXT__ 
#else
#define __CAN_EXT__ extern
#endif


#include <gd32f30x.h>


void can_networking_init(void);

int can0_send(uint8_t *data, uint8_t len);


#endif

