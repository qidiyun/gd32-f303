#define __CAN_C__

#include <gd32f30x.h>
#include "can.h"

//CAN 引脚初始化
void can_gpio_config(void)
{
	/* enable can clock */
	rcu_periph_clock_enable(RCU_CAN0);
	rcu_periph_clock_enable(RCU_GPIOD);
	
	/* configure CAN0 GPIO, CAN0_TX(PD1) and CAN0_RX(PD0) */
	gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_1);
	
	gpio_init(GPIOD, GPIO_MODE_IPU, GPIO_OSPEED_50MHZ, GPIO_PIN_0);
	
	gpio_pin_remap_config(GPIO_CAN_FULL_REMAP,ENABLE);
}

//CAN 控制器初始化
void __can_networking_init(void)
{
    can_parameter_struct can_parameter;
    can_filter_parameter_struct can_filter;
    /* initialize CAN register */
    can_deinit(CAN0);
    
    /* initialize CAN */
    can_parameter.time_triggered = DISABLE;
    can_parameter.auto_bus_off_recovery = DISABLE;
    can_parameter.auto_wake_up = DISABLE;
    can_parameter.auto_retrans = DISABLE;
    can_parameter.rec_fifo_overwrite = DISABLE;
    can_parameter.trans_fifo_order = DISABLE;
    can_parameter.working_mode = CAN_LOOPBACK_MODE;//CAN_NORMAL_MODE;
    can_parameter.resync_jump_width = CAN_BT_SJW_1TQ;
    can_parameter.time_segment_1 = CAN_BT_BS1_6TQ;
    can_parameter.time_segment_2 = CAN_BT_BS2_3TQ;
    /* baudrate 1Mbps */
    can_parameter.prescaler = 6;
    can_init(CAN0, &can_parameter);

    /* initialize filter */
    /* CAN0 filter number */
    can_filter.filter_number = 0;

    /* initialize filter */    
    can_filter.filter_mode = CAN_FILTERMODE_MASK;
    can_filter.filter_bits = CAN_FILTERBITS_32BIT;
    can_filter.filter_list_high = 0x0000;
    can_filter.filter_list_low = 0x0000;
    can_filter.filter_mask_high = 0x0000;
    can_filter.filter_mask_low = 0x0000;  
    can_filter.filter_fifo_number = CAN_FIFO0;
    can_filter.filter_enable = ENABLE;
    can_filter_init(&can_filter);
}

//CAN 接收中断设置
void can_nvic_config(void)
{
    /* configure CAN0 NVIC */
    nvic_irq_enable(USBD_LP_CAN0_RX0_IRQn,0,0);
}

//CAN初始化
void can_networking_init(void)
{
	//CAN 引脚初始化
	can_gpio_config();
	//CAN 控制器初始化
	__can_networking_init();
	//CAN 接收中断设置
	can_nvic_config();

	/* 使能 CAN 接收中断 */
    can_interrupt_enable(CAN0, CAN_INTEN_RFNEIE0);
}

//最多发送8个字节
int can0_send(uint8_t *data, uint8_t len)
{
	int ret;
	
	can_trasnmit_message_struct can_msg;

	if(len > 8)
	{
		return -1;
	}

	//标准格式帧标识符
    can_msg.tx_sfid = 0x321;
	//扩展格式帧标识符
    can_msg.tx_efid = 0x00;
	//指定帧格式、标准格式或扩展格式
    can_msg.tx_ft = CAN_FT_DATA;
	//指定帧类型、数据类型或远程类型
    can_msg.tx_ff = CAN_FF_STANDARD;
	//数据的长度
	can_msg.tx_dlen = len;
	//把数据复制过来
	memcpy(can_msg.tx_data, data, len);
	
	ret =  can_message_transmit(CAN0, &can_msg);

	return ret;
}


