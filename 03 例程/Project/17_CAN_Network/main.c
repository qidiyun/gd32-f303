/*!
    \file  main.c
    \brief CAN network communication demo
*/

/*
    Copyright (C) 2017 GigaDevice

    2017-05-19, V1.0.0, demo for GD32F30x
*/

#include "gd32f30x.h"
#include <stdio.h>
#include "gd32f303e_eval.h"


#include "can.h"


extern FlagStatus receive_flag;
extern can_receive_message_struct receive_message;
can_trasnmit_message_struct transmit_message;
    
void nvic_config(void);
void led_config(void);
ErrStatus can_networking(void);
void delay(void);


uint8_t can_test_data[2];

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    receive_flag = RESET;
    /* configure Tamper key */
    gd_eval_key_init(KEY_TAMPER, KEY_MODE_GPIO);
    /* configure USART */
    gd_eval_com_init(EVAL_COM1);
    /* configure leds */
    led_config();
    /* set all leds off */
    gd_eval_led_off(LED2);
    gd_eval_led_off(LED3);
    gd_eval_led_off(LED4);
    gd_eval_led_off(LED5);
	
    /* initialize CAN and CAN filter*/
    can_networking_init();
    
    /* initialize transmit message */
    transmit_message.tx_sfid = 0x321;
    transmit_message.tx_efid = 0x00;
    transmit_message.tx_ft = CAN_FT_DATA;
    transmit_message.tx_ff = CAN_FF_STANDARD;
    transmit_message.tx_dlen = 2;
    printf("\r\nplease press the Tamper key to transmit data!\r\n");
    while(1){
        /* waiting for the Tamper key pressed */
        while(0 == gd_eval_key_state_get(KEY_TAMPER)){
			#if 0
			transmit_message.tx_data[0] = 0xAB;
            transmit_message.tx_data[1] = 0xCD;
            printf("\r\nCAN0 transmit data: %x,%x\r\n", transmit_message.tx_data[0], transmit_message.tx_data[1]);
            /* transmit message */
            can_message_transmit(CAN0, &transmit_message);
			#endif
			can_test_data[0] = 0xAB;
			can_test_data[1] = 0xCD;
			can0_send(can_test_data, 2);
			
            delay();
            /* waiting for Tamper key up */
            while(0 == gd_eval_key_state_get(KEY_TAMPER));
        }
        if(SET == receive_flag){
            gd_eval_led_toggle(LED2);
            receive_flag = RESET;
            printf("\r\nCAN0 recive data: %x,%x\r\n", receive_message.rx_data[0], receive_message.rx_data[1]);
        }
    }
}


/*!
    \brief      delay
    \param[in]  none
    \param[out] none
    \retval     none
*/
void delay(void)
{
    uint16_t nTime = 0x0000;

    for(nTime = 0; nTime < 0xFFFF; nTime++){
    }
}

/*!
    \brief      configure the leds
    \param[in]  none
    \param[out] none
    \retval     none
*/
void led_config(void)
{
    gd_eval_led_init(LED2);
    gd_eval_led_init(LED3);
    gd_eval_led_init(LED4);
    gd_eval_led_init(LED5);
}



/* retarget the C library printf function to the usart */
int fputc(int ch, FILE *f)
{
    usart_data_transmit(EVAL_COM1, (uint8_t) ch);
    while(RESET == usart_flag_get(EVAL_COM1, USART_FLAG_TBE));
    return ch;
}
