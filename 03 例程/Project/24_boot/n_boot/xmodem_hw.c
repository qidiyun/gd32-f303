
#include "gd32f30x.h"
#include "all.h"
#include "xmodem.h"


//移植时，需要修改该函数
//串口收发，实用查询方式。
void xm_port_write(uint8 *ch)
{
	usart_data_transmit(USART0, (uint8_t)*ch);
    while(RESET == usart_flag_get(USART0, USART_FLAG_TBE));
}

//串口接收函数，需要移植
sint8 xm_port_read(uint8 *ch)
{
	if(RESET != usart_flag_get(USART0, USART_FLAG_RBNE))
	{
		
		*ch = (uint8_t)usart_data_receive(USART0);

		return 1;
	}
	
	return 0;
}

//在定时中断里调用该函数
//定时时间5ms
void xm_timer(void)
{
	xmodem_timeout++;
}


