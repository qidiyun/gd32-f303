#define _N_CPU_C_

#include "all.h"
#include "cpu_h.h"
#include "boot.h"
/************************************************************************
时间:2014.3.23
作者: lza1205

	基于stm32 的boot 该文件是cpu相关的代码，不同平台需要修改

************************************************************************/
uint8_t __cpu_goto_app(uint32_t Addr)
{
	pFunction Jump_To_Application;
	__IO uint32_t JumpAddress; 

	if (((*(__IO uint32_t*)Addr) & 0x2FFE0000 ) == 0x20000000)
	{ 
		/* 地址要偏移4 */
		JumpAddress = *(__IO uint32_t*) (Addr + 4);
		Jump_To_Application = (pFunction) JumpAddress; 

		/* 使用app的栈 */
		__set_MSP(*(__IO uint32_t*) Addr);

		/* 跳转到用户函数入口地址 */
		Jump_To_Application();
	}
	return 1;
}

//1秒产生一次中断 int_time = CLK / ((prescaler+1) *  (period+1))
//CLK = 60MHz
void timer1_init(void)
{
    timer_parameter_struct timer_parameter;

		rcu_periph_clock_enable(RCU_TIMER1);

		//预分频
    timer_parameter.prescaler = 1999,
    //对齐模式
    timer_parameter.alignedmode = TIMER_COUNTER_EDGE,
    //定时器增长方向
    timer_parameter.counterdirection = TIMER_COUNTER_UP,
    //定时器自动加载值
    timer_parameter.period = 599,
    //时钟分频值
    timer_parameter.clockdivision = TIMER_CKDIV_DIV4,

    timer_init(TIMER1, &timer_parameter);
   
    timer_interrupt_enable(TIMER1, TIMER_INT_UP);
    nvic_irq_enable(TIMER1_IRQn, 0, 2);
  
    timer_enable(TIMER1);
}


void TIMER1_IRQHandler()
{
    if (timer_interrupt_flag_get(TIMER1, TIMER_INT_UP) != RESET)
	{
			g_delay_ms --;
			xm_timer();
    	timer_interrupt_flag_clear(TIMER1, TIMER_INT_UP);
    }
}


/*!
    \brief      this function handles USART0 exception
    \param[in]  none
    \param[out] none
    \retval     none
*/
void USART0_IRQHandler(void)
{
	uint8_t ch = 0;
	if(RESET != usart_interrupt_flag_get(USART0, USART_INT_FLAG_RBNE))
	{
		g_compel_out = 1;
		
		ch = (uint8_t)usart_data_receive(USART0);

		receive_input(ch);
	}
}

/*********************************************************
	经过测试，在跳转之前，要先将前面已经打开的中断都屏蔽掉
	因为跳转过去后，使用的是新的向量表，而新的向量表可能
	没有对应的中断，这样会出错
*********************************************************/
void __cpu_disable_int(void)
{
	nvic_irq_disable(TIMER1_IRQn);
	nvic_irq_disable(USART0_IRQn);
}


void boot_cpu_init(void)
{
	timer1_init();
}
